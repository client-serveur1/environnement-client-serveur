# Environnement-client-serveur

Repo gitlab pour le cours environnement client/serveur.
Membres : Loris COCCO et Mathias THUILLIER

## Environnement Client / Serveur :
Séance de 9h-12h30 du 19/04/2022 :
Lecture du sujet, modélisation de la base de données (Modèle entité association et modèle physique des données).
Lien du MES : https://app.diagrams.net/?libs=general;uml#Aclient-serveur1%2Fenvironnement-client-serveur%2Fmain%2FMod%C3%A8le%20entit%C3%A9%20association
Lien du MPD : https://app.diagrams.net/#Aclient-serveur1%2Fenvironnement-client-serveur%2Fmain%2FUntitled%20Diagram.drawio
