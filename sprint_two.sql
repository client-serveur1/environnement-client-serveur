ALTER TABLE `gardens_areas` 
  ADD COLUMN `longitude` DECIMAL(5,6),
  ADD COLUMN `latitude` DECIMAL(5,6),
  ADD COLUMN `width` INT,
  ADD COLUMN `height` INT;

DROP TABLE IF EXISTS `notes`;
CREATE TABLE IF NOT EXISTS `notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('text','audio')  NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `audioLink` text DEFAULT NULL,
  `date` timestamp DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

DELIMITER $$
CREATE TRIGGER `note_creation_check_fields` BEFORE INSERT ON `notes` FOR EACH ROW BEGIN
 IF (new.type = "text" AND new.description IS NULL) THEN
  SIGNAL SQLSTATE '45000'
  SET MESSAGE_TEXT = 'Error';
 END IF;
 IF (new.type = "audio" AND new.audioLink IS NULL) THEN
  SIGNAL SQLSTATE '45000'
  SET MESSAGE_TEXT = 'Error'; END IF;
END 
$$
DELIMITER ;

ALTER TABLE gardens_areas 
ADD COLUMN noteId int,
ADD FOREIGN KEY (noteId) REFERENCES notes (id);

ALTER TABLE plants 
ADD COLUMN noteId int,
ADD FOREIGN KEY (noteId) REFERENCES notes (id);

ALTER TABLE seeds 
ADD COLUMN noteId int,
ADD FOREIGN KEY (noteId) REFERENCES notes (id);

CREATE VIEW precarious_plants AS SELECT 
  s.name AS 'Nom de la plante', 
  p.tempMin AS 'Température minimale de la plante', 
  h.minTemp AS 'Température minimale de la zone' 
  FROM plants p 
  LEFT JOIN seeds s ON (s.id = p.seedId) 
  LEFT JOIN gardens_areas ga ON (p.gardenAreaId = ga.id) 
  LEFT JOIN hardinesses h ON (h.id = ga.hardinessId) 
  WHERE p.tempMin < h.minTemp;

CREATE TABLE plants_name(
  id int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT, 
  name varchar(150),
  plantId int(11), 
  FOREIGN KEY (plantId) REFERENCES plants(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE plants ADD COLUMN tempMin DECIMAL(4,2);

START TRANSACTION;
CREATE TABLE type_action(
  id int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT, 
  name varchar(255),
  description varchar(255),
  picture varchar(255), 
  typeId int(11),
 FOREIGN KEY (typeId) REFERENCES type_action(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE actions(
  id int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT, 
  status ENUM("TODO", "EXECUTED", "ABANDONED"), 
  frequency int(11), 
  dateMin datetime, 
  dateMax datetime, 
  gardenId int(11), 
  typeId int(11), 
  userId int(11), 
  FOREIGN KEY (gardenId) REFERENCES gardens(id), 
  FOREIGN KEY (typeId) REFERENCES type_action(id), 
  FOREIGN KEY (userId) REFERENCES users(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE actions_note (
  actionId int(11), 
  noteId int(11), 
  PRIMARY KEY(actionId, noteId),
  FOREIGN KEY (actionId) REFERENCES actions(id),
  FOREIGN KEY (noteId) REFERENCES notes(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
COMMIT;

CREATE USER 'demo'@'%';
GRANT SELECT, SHOW VIEW ON i_garden.* TO 'demo'@'%';

-- SELECT GROUP_CONCAT( v.name ORDER BY v.name ASC SEPARATOR ', ') AS 'Noms des familles', ga.name AS 'Nom de la zone' FROM gardens_areas ga LEFT JOIN plants p ON (ga.id = p.gardenAreaId) LEFT JOIN seeds s ON (s.id = p.seedId) LEFT JOIN varieties v ON (v.id = s.varietyId) LEFT JOIN plants_history ph ON (ph.plantid = p.id) GROUP BY ga.name

-- DELIMITER $$
-- CREATE TRIGGER `on_user_delete_purge` AFTER DELETE ON `users` FOR EACH ROW BEGIN
--   SELECT id INTO @seedsIds FROM seeds WHERE userId = 1;
--   SELECT noteId INTO @notesIds FROM plants WHERE seedId IN (@seedsIds);
-- END 
-- $$
-- DELIMITER ;
