-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mar. 19 avr. 2022 à 11:46
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `i_garden`
--

-- --------------------------------------------------------

--
-- Structure de la table `gardens`
--

DROP TABLE IF EXISTS `gardens`;
CREATE TABLE IF NOT EXISTS `gardens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `zipCode` int(11) NOT NULL,
  `hardinessId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hardinessId` (`hardinessId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `gardens_areas`
--

DROP TABLE IF EXISTS `gardens_areas`;
CREATE TABLE IF NOT EXISTS `gardens_areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gardenId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `soil` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `plantId` int(11) NOT NULL,
  `hardinessId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `gardenId` (`gardenId`),
  KEY `plantId` (`plantId`),
  KEY `hardinessId` (`hardinessId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `hardinesses`
--

DROP TABLE IF EXISTS `hardinesses`;
CREATE TABLE IF NOT EXISTS `hardinesses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `minTemp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `plants`
--

DROP TABLE IF EXISTS `plants`;
CREATE TABLE IF NOT EXISTS `plants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sowingDate` datetime NOT NULL,
  `transplantingDate` datetime NOT NULL,
  `bloomingDate` datetime NOT NULL,
  `cropingDate` datetime NOT NULL,
  `cropingQuantity` int(11) NOT NULL,
  `seedId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `seedId` (`seedId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `plants_history`
--

DROP TABLE IF EXISTS `plants_history`;
CREATE TABLE IF NOT EXISTS `plants_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plantId` int(11) NOT NULL,
  `statusId` int(11) NOT NULL,
  `gardenAreaId` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `lifeCycleId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `gardenAreaId` (`gardenAreaId`),
  KEY `plantId` (`plantId`),
  KEY `statusId` (`statusId`),
  KEY `lifeCycleId` (`lifeCycleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `plants_life_cycle`
--

DROP TABLE IF EXISTS `plants_life_cycle`;
CREATE TABLE IF NOT EXISTS `plants_life_cycle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plantId` int(11) NOT NULL,
  `cycle` enum('germination','growth','bloom','fructification','dead') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `plantId` (`plantId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `plants_status`
--

DROP TABLE IF EXISTS `plants_status`;
CREATE TABLE IF NOT EXISTS `plants_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plantId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `status` enum('active','passed','treadted','') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `plantId` (`plantId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `seeds`
--

DROP TABLE IF EXISTS `seeds`;
CREATE TABLE IF NOT EXISTS `seeds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `varietyId` int(11) NOT NULL,
  `classification` enum('lively','annuel','biennal','') NOT NULL,
  `maxHeigh` double NOT NULL,
  `pic` text NOT NULL,
  `description` text NOT NULL,
  `latinName` varchar(255) NOT NULL,
  `icon` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `varietyId` (`varietyId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` blob NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `users_gardens`
--

DROP TABLE IF EXISTS `users_gardens`;
CREATE TABLE IF NOT EXISTS `users_gardens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gardenId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `gardenId` (`gardenId`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `varieties`
--

DROP TABLE IF EXISTS `varieties`;
CREATE TABLE IF NOT EXISTS `varieties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `varietyId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `varietyId` (`varietyId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `gardens`
--
ALTER TABLE `gardens`
  ADD CONSTRAINT `gardens_ibfk_1` FOREIGN KEY (`hardinessId`) REFERENCES `hardinesses` (`id`);

--
-- Contraintes pour la table `gardens_areas`
--
ALTER TABLE `gardens_areas`
  ADD CONSTRAINT `gardens_areas_ibfk_1` FOREIGN KEY (`gardenId`) REFERENCES `gardens` (`id`),
  ADD CONSTRAINT `gardens_areas_ibfk_2` FOREIGN KEY (`plantId`) REFERENCES `plants` (`id`),
  ADD CONSTRAINT `gardens_areas_ibfk_3` FOREIGN KEY (`hardinessId`) REFERENCES `hardinesses` (`id`);

--
-- Contraintes pour la table `plants_history`
--
ALTER TABLE `plants_history`
  ADD CONSTRAINT `plants_history_ibfk_1` FOREIGN KEY (`plantId`) REFERENCES `plants` (`id`),
  ADD CONSTRAINT `plants_history_ibfk_3` FOREIGN KEY (`gardenAreaId`) REFERENCES `gardens_areas` (`id`),
  ADD CONSTRAINT `plants_history_ibfk_4` FOREIGN KEY (`statusId`) REFERENCES `plants_status` (`id`),
  ADD CONSTRAINT `plants_history_ibfk_5` FOREIGN KEY (`lifeCycleId`) REFERENCES `plants_life_cycle` (`id`);

--
-- Contraintes pour la table `plants_life_cycle`
--
ALTER TABLE `plants_life_cycle`
  ADD CONSTRAINT `plants_life_cycle_ibfk_1` FOREIGN KEY (`plantId`) REFERENCES `plants` (`id`);

--
-- Contraintes pour la table `plants_status`
--
ALTER TABLE `plants_status`
  ADD CONSTRAINT `plants_status_ibfk_1` FOREIGN KEY (`plantId`) REFERENCES `plants` (`id`);

--
-- Contraintes pour la table `seeds`
--
ALTER TABLE `seeds`
  ADD CONSTRAINT `seeds_ibfk_1` FOREIGN KEY (`id`) REFERENCES `plants` (`seedId`);

--
-- Contraintes pour la table `users_gardens`
--
ALTER TABLE `users_gardens`
  ADD CONSTRAINT `users_gardens_ibfk_1` FOREIGN KEY (`gardenId`) REFERENCES `gardens` (`id`),
  ADD CONSTRAINT `users_gardens_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `varieties`
--
ALTER TABLE `varieties`
  ADD CONSTRAINT `varieties_ibfk_1` FOREIGN KEY (`id`) REFERENCES `seeds` (`varietyId`),
  ADD CONSTRAINT `varieties_ibfk_2` FOREIGN KEY (`varietyId`) REFERENCES `varieties` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
